#!/bin/bash
exec 3<$1
exec 4>$2
nPersonID=0
nHouseHold=0
tempHouseHold=0
totalHouseHold=0
while read -u 3 houseHoldID personID age gender
do
	if [ $gender == 2 ]; then
		if [ $age -ge 30 ] && [ $age -le 50 ]; then
			echo $personID >&4
			((nPersonID++))
		fi
		
		if [ $tempHouseHold == $houseHoldID ]; then
			((nHouseHold++))
		elif [ $nHouseHold -gt 2 ]; then
			((totalHouseHold++))	
			nHouseHold=0
		fi
		tempHouseHold=$houseHoldID
	fi
done
echo "Total Person ID: $nPersonID"
echo "Total Household ID: $totalHouseHold"
